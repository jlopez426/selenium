import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Test {

    @FindBy(xpath = "input[@id='email']")
    static WebElement x;
    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver","D:\\Programas\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        WebDriverWait wait = new WebDriverWait(driver, 3000);

        driver.get("http://www.facebook.com");
        wait.until(ExpectedConditions.visibilityOf(x));
    }
}